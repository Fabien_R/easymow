import org.scalatest._
import Matchers._
import scala.util.{Failure, Success}

class TestOrientation extends FlatSpec {

  "From North" should "be WEST or EAST" in {
    NORTH.L should be (WEST)
    NORTH.R should be (EAST)
  }

  "From EAST" should "be NORTH or SOUTH" in {
    EAST.L should be (NORTH)
    EAST.R should be (SOUTH)
  }

  "From SOUTH" should "be EAST or WEST" in {
    SOUTH.L should be (EAST)
    SOUTH.R should be (WEST)
  }

  "From WEST" should "be SOUTH or NORTH" in {
    WEST.L should be (SOUTH)
    WEST.R should be (NORTH)
  }

  "Print litteral" should "be N,E,S or W" in {
    NORTH.printLitteral should be ("N")
    EAST.printLitteral should be ("E")
    SOUTH.printLitteral should be ("S")
    WEST.printLitteral should be ("W")
  }

  "String to orientation" should "be NORTH, EAST, SOUTH or WEST" in {
    Orientation.stringToOrientation("N") should be (NORTH)
    Orientation.stringToOrientation("E") should be (EAST)
    Orientation.stringToOrientation("S") should be (SOUTH)
    Orientation.stringToOrientation("W") should be (WEST)
  }

}
