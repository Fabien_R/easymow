import org.scalatest.Matchers._
import org.scalatest._

import scala.util.{Failure, Success}

class TestParser extends FlatSpec {
  "read file" should "be success or failure" in {
    val tryFileSuccess = Parser.readFile("script.txt")

    tryFileSuccess should be (Success(List()))
  }

  "create board" should "be success or failure" in {
    val tryBoardSuccess = Parser.createBoard(List("5 5"))

    tryBoardSuccess should be (Success(Board(5,5)))
  }

  "create shearer" should "be success or failure" in {
    val tryShearerSuccess = Parser.createShearer("5 5 N")

    tryShearerSuccess should be (Success(Shearer(5,5, NORTH)))
  }


}
