import org.scalatest.Matchers._
import org.scalatest._

import scala.util.{Failure, Success}

class TestShearer extends FlatSpec {

  val shearerNorth = Shearer(5, 5, NORTH)
  val shearerEast = Shearer(5, 5, EAST)
  val shearerSouth = Shearer(5, 5, SOUTH)
  val shearerWest = Shearer(5, 5, WEST)
  val board = Board(6,6)

  "move" should "move the shearer" in {

    shearerNorth.move(board) should be (Shearer(5, 6, NORTH))
    shearerEast.move(board) should be (Shearer(6, 5, EAST))
    shearerSouth.move(board) should be (Shearer(5, 4, SOUTH))
    shearerWest.move(board) should be (Shearer(4, 5, WEST))
  }

  "change orientation right" should "turn right" in {
    shearerNorth.changeOrientation("D") should be (Shearer(5, 5, EAST))
    shearerEast.changeOrientation("D") should be (Shearer(5, 5, SOUTH))
    shearerSouth.changeOrientation("D") should be (Shearer(5, 5, WEST))
    shearerWest.changeOrientation("D") should be (Shearer(5, 5, NORTH))
  }

  "change orientation left" should "turn left" in {
    shearerNorth.changeOrientation("G") should be (Shearer(5, 5, WEST))
    shearerEast.changeOrientation("G") should be (Shearer(5, 5, NORTH))
    shearerSouth.changeOrientation("G") should be (Shearer(5, 5, EAST))
    shearerWest.changeOrientation("G") should be (Shearer(5, 5, SOUTH))
  }

}
