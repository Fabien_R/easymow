import org.scalatest.Matchers._
import org.scalatest._

class TestBoard extends FlatSpec {

  "is include" should "be true or false" in {
    val board = Board(5, 5)
    board.isIncluded(6,6) should be (false)
    board.isIncluded(4,6) should be (false)
    board.isIncluded(6,4) should be (false)

    board.isIncluded(3,2) should be (true)
    board.isIncluded(5,5) should be (true)
  }

}
