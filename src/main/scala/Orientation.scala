trait Orientation{
  def L : Orientation
  def R : Orientation
  def printLitteral : String
}

object Orientation{
  def stringToOrientation(orientation: String) = {
    orientation match {
      case "N" => NORTH
      case "E" => EAST
      case "S" => SOUTH
      case "W" => WEST
    }
  }
}
object NORTH extends Orientation {
  override def L = WEST
  override def R = EAST
  override def printLitteral = "N"
}

object EAST extends Orientation {
  override def L = NORTH
  override def R = SOUTH
  override def printLitteral = "E"
}

object SOUTH extends Orientation {
  override def L = EAST
  override def R = WEST
  override def printLitteral = "S"
}

object WEST extends Orientation {
  override def L = SOUTH
  override def R = NORTH
  override def printLitteral = "W"
}

