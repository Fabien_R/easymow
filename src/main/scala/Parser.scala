import scala.io.Source
import scala.util.Try

object Parser {
  def readFile(name: String) = Try(Source.fromResource(name).getLines().toList)

  def createBoard(lines: List[String]) = {
    Try {
      val values = lines(0).split(" ")
      Board(values(0).toInt, values(1).toInt)
    }
  }

  def createShearer(line: String) = {
    Try {
      val values = line.split(" ")
      Shearer(values(0).toInt, values(1).toInt, Orientation.stringToOrientation(values(2)))
    }
  }
}
