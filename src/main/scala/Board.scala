//i: y
//j: x
case class Board(i: Int, j: Int){

  def isIncluded(x: Int, y: Int) = x <= j && y <= i
}
