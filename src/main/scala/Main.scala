import scala.util.{Failure, Success}

object Main {
  def main(args: Array[String]): Unit = {
    Parser.readFile("script.txt") match {
      case Success(lines) =>  println(Process.processBoard(lines))
      case Failure(_) => println("Cannot open file")
    }
  }
}
