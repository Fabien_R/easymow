import scala.util.{Failure, Success}

object Process {
  def processBoard(lines: List[String]) = {
    val tryBoard = Parser.createBoard(lines)
    tryBoard match {
      case Success(board) => {
        println("START\n")
        processLines(lines.drop(1), board)
      }
      case Failure(_) => println("Unable to create board")
    }
  }

  def processLines(lines: List[String], board: Board): Unit = {
    lines match {
      case shearer :: actions :: rest => {
        processShearer(shearer, actions, board)
        processLines(rest, board)
      }
      case shearer :: rest => println("Malformed script")
      case isEmpty => println("\nEND")
    }
  }

  def processShearer(shearerLine: String, actions: String, board: Board) = {
    val tryShearer = Parser.createShearer(shearerLine)
    tryShearer match {
      case Success(shearer) => processActions(shearer, actions.split("").toList, board)
      case Failure(_) => println("Unable to create shearer")
    }
  }

  def processActions(shearer: Shearer, actions: List[String], board: Board) : Unit = {
    actions match {
      case action :: rest => processActions(shearer.action(action, board), rest, board)
      case _ => println(shearer.x + " " + shearer.y + " " + shearer.orientation.printLitteral)
    }
  }
}
