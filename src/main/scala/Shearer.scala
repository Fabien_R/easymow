case class Shearer(x: Int, y: Int, orientation: Orientation){

  def move(board: Board) = {
    orientation match {
      case NORTH => if(board.isIncluded(x, y + 1)) copy(x, y + 1, orientation) else this
      case EAST => if(board.isIncluded(x + 1, y)) copy(x + 1, y, orientation) else this
      case SOUTH => if(board.isIncluded(x, y - 1)) copy(x, y - 1, orientation) else this
      case WEST => if(board.isIncluded(x - 1, y)) copy(x - 1, y, orientation) else this
      case _ => this
    }
  }

  def changeOrientation(turn: String) = {
    turn match {
      case "D" => copy(x, y, orientation.R)
      case "G" => copy(x, y, orientation.L)
      case _ => this
    }
  }

  def action(action: String, board: Board) ={
    action match {
      case "A" => move(board)
      case "D" | "G" => changeOrientation(action)
      case _ => println("Unknown action : " + action); this
    }
  }
}
