# **Projet EasyMow**

## Table des matières

* [Introduction](#introduction)
* [Les choix d'implémentation](#choix)
    * [La pelouse](#board)
    * [La tondeuse](#shearer)
    * [Les directions (Orientation)](#orientation)
    * [La gestion du script de commandes (Parser)](#parser)
    * [L'exécution du programme (Process)](#process)
* [Compiler le programme](#compiler)
* [Exécuter le programme](#exécuter)
* [Tester le programme](#tester)

## Introduction <i id="introduction"></i>

La société EasyMow a décidé de développer une tondeuse à gazon automatique, destinée aux surfaces rectangulaires.

La tondeuse peut être programmée pour parcourir l'intégralité de la surface.
La position de la tondeuse est représentée par une combinaison de coordonnées et d'une lettre indiquant l'orientation selon la notation cardinale anglaise. La pelouse est divisée en grille pour simplifier la navigation.
## Les choix d'implémentation <i id="choix"></i>
Afin de répondre aux besoins de la société EasyMow, un certain nombre de classes et objets ont été définis : 

| La pelouse | La tondeuse | Les directions | La gestion du script de commandes | L'exécution du programme |
| ------ | ----------- | ---------------- | -----------| ------- |
| case class Board | case class Shearer | trait Orientation ainsi que objects NORTH, EAST, SOUTH et WEST  | object Parser | object Process |

### La pelouse (Board) <i id="board"></i>
La pelouse est représentée par la case class Board.  
La pelouse est définie par deux entiers : sa longueur et sa largeur.

Il est possible d'appeler une méthode "isInclude" afin de savoir si des coordonnées sont incluses ou non dans la pelouse. Cette méthode est notamment utilisée lors que la tondeuse bouge sur la pelouse.

### La tondeuse (Shearer) <i id="shearer"></i>
Une tondeuse est représentée par la case class Shearer.
Une tondeuse est définie par deux entiers (x, y) ainsi que par une orientation.

Le choix d'utiliser une case class est en partie justifié par le fait que le programme effectue un nombre important de match / case sur les coordonnées et orientation de la tondeuse.

Il est possible d'appeler trois méthode sur une tondeuse :
* **move** : une nouvelle tondeuse ayant avancée d'une case est renvoyée.
* **changeOrientation** : une nouvelle tondeuse avec l'orientation indiquée en paramètre est renvoyée.
* **action** : en fonction de l'action passée en paramètre, appelle soit move ou changeOrientation.

### Les directions (Orientation) <i id="orientation"></i>
L'orientation d'une tondeuse est définie par un trait Orientation. Cette "interface" nécessite d'implémenter trois méthodes :

* **D** : renvoi l'orientation correspondant à un virage vers la droite
* **G** : renvoi l'orientation correspondant à un virage vers à gauche.
* **printLitteral** : En fonction de l'orientation, renvoi le littéral correspondant.

Ce trait est hérité par quatre objects : **NORTH**, **EAST**, **SOUTH**, **WEST**. Des objects ont été choisis car ils sont utilisés pour stocker des constantes.

### La gestion du script de commandes (Parser)<i id="parser"></i>
La gestion du script de commandes se fait depuis l'object Parser.
L'utilisation d'un object est justifié par le fait qu'un object est un singleton (instance unique) et qu'il n'était pas necssaire d'avoir une autre entité pour gérer le fichier.

Il est possible d'appeler trois méthodes depuis un object Parser :
* **readFile** : reçoit un nom de fichier en paramètre situé dans le dossier "resources", la méthode essaie de l'ouvrir et de le renvoyer
* **createBoard** : depuis une ligne du fichier, la méthode essaie de créer une pelouse et de la renvoyer.
* **createShearer** : depuis une ligne du fichier, la méthode essaie de créer une tondeuse et de la renvoyer. 

### L'exécution du programme (Process)<i id="process"></i>
L'exécution de la simulation du déplacement de la tondeuse sur la pelouse est effectuée depuis un object Parser. Un object a été utilisé pour les mêmes raison que le Parser.

Il est possible d'appeler quatre méthodes depuis un object Process :
* **processBoard** : reçoit toutes les lignes du fichier de script, appelle la méthode createBoard du Parser puis en cas de succès, elle appelle la méthode processLines. 
* **processLines** : reçoit le tableau de ligne restantes. Il faire ces lignes 2 à 2 car elles contiennent les caractérisques initiales de la tondeuse et les actions a effectué. Le tableau est décomposé et la méthode processShearer est appelée. Enfin, la méthode processLines se rappelle récursivement afin de traiter toutes les lignes du tableau.
* **processShearer** : essaie de créer une tondeuse depuis la ligne passée en paramètre. En cas de succès, la méthode processActions est appelée avec la liste d'action a effectué sur la tondeuse.
* **processActions** : méthode récursive qui consomme un litteral dans la liste des actions. Quand la liste est vide, les caractérisques de la tondeuse sont affichées à l'utilisateur.

## Compiler le programme <i id="compiler"></i>
EasyMow est un projet sbt classique. Ainsi, il est possible de compiler le projet depuis le terminal sbt en utilisant la commande **compile**. Il est également possible de compiler le projet en l'important dans un IDE comme Eclipse ou IntelliJ. 

## Exécuter le programme <i id="exécuter"></i>
Depuis le terminal sbt, il faut saisir la commande **run** sans argument. Si le projet est importé dans un IDE, il faut exécuter la méthode main de l'Object Main.

## Tester le programme <i id="tester"></i>
Depuis le terminal sbt, il faut saisir la commande **test** afin de lancer tous les tests du programme. Il est possible de lancer individuellement chaque test lorsque le projet est importé dans un IDE comme Eclipse ou IntelliJ.

NB : l'Object Process ne possède pas de test. En effet, les méthodes ne renvoyant aucune valeur, cela n'avait pas de sens de leur rédiger des tests.